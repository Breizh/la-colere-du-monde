/*
==============================================================
   - La colère du monde -
  author :      Breizh
  description : Outils pour calculer l'apparition
  .             de monstre en fonction des déplacement
==============================================================
*/

// Probabilité par case
var probaParCase; // Valeur en pourcentage 

var phrase = ["Les aventuriers se déplace",
              "Les compère avance",
              "La dreamteam bouge",
              "Le groupe voyage",
              "Les amis vagabonde",
              "La companie explore",
              "Les camarade sillonne",
              "Les compagnons marche",
              "Le bard part en eclaireur",
              "La troupe déambule"]

var numPhrase = 0;

function searchEnter(key) {
  var x = key.which || key.keyCode;
  if (x == 13) {
    search();
  }
}

function search() {
  var nbCase = parseInt(document.getElementById("bar-recherche").value, 10);
  if (!Number.isInteger(nbCase)) {
    window.alert("La valeur n\'est pas un entier");
  }

  else {
    var pourcent = parseInt(document.getElementById("bar-pourcent").value, 10);

    if (!Number.isInteger(pourcent) || !(pourcent >= 0 && pourcent <= 100)) {
      window.alert("La valeur est invalide");
    }
    else {
      probaParCase = pourcent;

      // crée un nouvel élément div
      var responseField = document.getElementById("bloc-resultats");
      responseField.innerHTML = "";

      var p = document.createElement("p");
      proba = nbCase * probaParCase;
      if (entierAleatoire(0, 100) <= proba) {
        p.innerHTML = phrase[numPhrase] + " et un mob apparait !";
        if (numPhrase == phrase.length - 1) {
          numPhrase = 0;
        }
        else {
          numPhrase = numPhrase + 1;
        }
      }
      else {
        p.innerHTML = phrase[numPhrase] + ", le monde est calme.";
        if (numPhrase == phrase.length - 1) {
          numPhrase = 0;
        }
        else {
          numPhrase = numPhrase + 1;
        }
      }
      // ajoute le nœud texte au nouveau div créé
      responseField.append(p);
    }
  }
}


// tools

function entierAleatoire(min, max) {
 return Math.floor(Math.random() * (max - min + 1)) + min;
}
